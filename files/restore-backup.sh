GITLAB_TAR=`/usr/local/bin/aws s3 ls s3://belligerence-gitlab/ | tail -1 | awk '{print $4}'`
BACKUP_NUMBER="${GITLAB_TAR%%_*}"

/usr/local/bin/aws s3 cp s3://belligerence-gitlab/${BACKUP_NUMBER}_gitlab_backup.tar /home/ec2-user

sleep 10
docker cp /home/ec2-user/${BACKUP_NUMBER}_gitlab_backup.tar gitlab:/var/opt/gitlab/backups/${BACKUP_NUMBER}_gitlab_backup.tar
sleep 10
docker exec gitlab gitlab-ctl stop unicorn
docker exec gitlab gitlab-ctl stop sidekiq
docker exec gitlab gitlab-ctl status
docker exec gitlab gitlab-rake gitlab:backup:restore force=yes BACKUP=${BACKUP_NUMBER}
sleep 30
docker exec gitlab gitlab-ctl start
sleep 40
docker exec gitlab gitlab-rake gitlab:check SANITIZE=true

rm /home/ec2-user/${BACKUP_NUMBER}_gitlab_backup.tar
